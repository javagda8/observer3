package com.sda.oo;

import javax.swing.text.View;

public class ObserverExample {
    public static void main(String[] args) {
        NewsStation station = new NewsStation();

        Viewer marian = new Viewer(11, "Marian");
        Viewer szczepan = new Viewer(9, "Szczepan");

        station.addObserver(marian);
        station.addObserver(szczepan);

        station.addNews("Pali sie bialy dom!", 10);

    }
}
